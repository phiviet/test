"use strict";
exports.__esModule = true;
var file_model_1 = require("./../models/file.model");
var shared_1 = require("../shared");
var repositories_1 = require("../repositories");
var path = require("path");
var fs = require("fs");
var uuid = require("uuid");
var FileService = /** @class */ (function () {
    function FileService() {
    }
    FileService.getInstance = function () {
        if (!FileService.instance) {
            FileService.instance = new FileService();
        }
        return FileService.instance;
    };
    FileService.prototype.getAllFiles = function (search) {
        return repositories_1.dbContext.getFileRepository()
            .getAllFiles(search)
            .then(function (files) {
            return Promise.resolve(files);
        })["catch"](function (err) {
            return Promise.reject(err);
        });
    };
    FileService.prototype.update = function (fileFolder, id) {
        var folder = {
            name: fileFolder.name,
            id: id
        };
        return repositories_1.dbContext.getFileRepository()
            .update(folder)
            .then(function (file) {
            delete file.content;
            return Promise.resolve(file);
        })["catch"](function (err) {
            return Promise.reject(err);
        });
    };
    FileService.prototype.createFolder = function (file, parentId) {
        var folder = {
            name: file.name,
            isFile: false,
            format: '',
            content: '',
            path: ''
        };
        return repositories_1.dbContext.getFileRepository()
            .getIndexNext(parentId)
            .then(function (nextIndex) {
            return new Promise(function (res, rej) {
                folder.index = nextIndex;
                repositories_1.dbContext.getFileRepository()
                    .insert(folder)
                    .then(function (newFoler) {
                    res(newFoler);
                })["catch"](function (err) {
                    rej({
                        message: err.message
                    });
                });
            });
        })
            // tslint:disable-next-line:no-shadowed-variable
            .then(function (file) {
            return Promise.resolve(file);
        })["catch"](function (err) {
            return Promise.reject(err);
        });
    };
    FileService.prototype.upload = function (request) {
        return saveFileToServer(request).then(function (req) {
            // tslint:disable-next-line:prefer-const
            var publicFolderPath = path.join(__dirname, '..', 'public/');
            // tslint:disable-next-line:prefer-const
            var fileName = req['path'];
            return shared_1.getMetaData(publicFolderPath + fileName).then(function (meta) {
                return saveNewFile();
                function saveNewFile() {
                    return repositories_1.dbContext.getFileRepository()
                        .getIndexNext(request['parentId'])
                        .then(function (newIndex) {
                        var newFile = standardizeFileData(request);
                        newFile.index = newIndex;
                        newFile.content = meta.text;
                        return repositories_1.dbContext.getFileRepository()
                            .saveFile(newFile)
                            .then(function (file) {
                            return Promise.resolve(file);
                        })["catch"](function (err) {
                            console.log(err);
                            return Promise.reject(err);
                        });
                    });
                }
            });
        });
        // tslint:disable-next-line:no-shadowed-variable
        function standardizeFileData(request) {
            var newFile = new file_model_1.FileModel();
            newFile.name = request.files.name.replace(/\.[^/.]+$/, '');
            newFile.format = request.format;
            newFile.path = request.path;
            newFile.isFile = true;
            return newFile;
        }
        // tslint:disable-next-line:no-shadowed-variable
        function saveFileToServer(request) {
            return new Promise(function (resolve, reject) {
                // tslint:disable-next-line:prefer-const
                var publicFolderPath = path.join(__dirname, '..', 'public/');
                if (!fs.existsSync(publicFolderPath)) {
                    fs.mkdirSync(publicFolderPath);
                }
                var file = request.files;
                var uuidFileName = uuid();
                var extFile = getFormatFormFileName(file.name);
                var savePath = uuidFileName + '.' + extFile['0'].toLowerCase();
                var filePath = publicFolderPath + savePath;
                file.mv(filePath, function (err, data) {
                    if (err) {
                        console.log(err);
                        return reject(err);
                    }
                    request.path = savePath;
                    request.format = extFile;
                    resolve(request);
                });
            });
        }
        function getFormatFormFileName(fileName) {
            return (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;
        }
    };
    FileService.prototype.getFile = function (id) {
        return repositories_1.dbContext.getFileRepository()
            .findOne(id)
            .then(function (responsedFile) {
            return Promise.resolve(responsedFile);
        })["catch"](function (err) {
            return Promise.reject(err);
        });
    };
    FileService.prototype.getParent = function (id) {
        return repositories_1.dbContext.getFileRepository()
            .getParents(id)
            .then(function (responsedFiles) {
            return Promise.resolve(responsedFiles);
        })["catch"](function (err) {
            return Promise.reject(err);
        });
    };
    FileService.prototype["delete"] = function (id) {
        return repositories_1.dbContext.getFileRepository()["delete"](id)
            .then(function (response) {
            return Promise.resolve(response);
        })["catch"](function (err) {
            return Promise.reject(err);
        });
    };
    return FileService;
}());
exports.FileService = FileService;
exports.fileService = FileService.getInstance();
