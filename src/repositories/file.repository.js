"use strict";
exports.__esModule = true;
var models_1 = require("../models");
var FileRepository = /** @class */ (function () {
    function FileRepository() {
    }
    FileRepository.prototype.findOne = function (id) {
        return models_1.FileModel
            .findOne({
            _id: id
        }).then(function (responsedFile) {
            return Promise.resolve(responsedFile);
        })["catch"](function (err) {
            return Promise.reject({
                message: err.message
            });
        });
    };
    FileRepository.prototype.getParents = function (id) {
        var _this = this;
        return models_1.FileModel
            .findOne({
            _id: id
        }).then(function (responsedFile) {
            var index = responsedFile.index;
            var parentsIndex = _this.getParentsIndex(index);
            if (parentsIndex.length === 0) {
                return Promise.resolve([]);
            }
            else {
                var query = _this.buildQueryGetParents(parentsIndex);
                return models_1.FileModel
                    .find(query, { name: 1, index: 1, _id: 1 })
                    .sort({ index: 1 })
                    .then(function (folders) {
                    return Promise.resolve(folders.map(function (folder) { return folder.toObject(); }));
                })["catch"](function (err) {
                    return Promise.reject({
                        message: err.message
                    });
                });
            }
        })["catch"](function (err) {
            return Promise.reject({
                message: err.message
            });
        });
    };
    FileRepository.prototype.buildQueryGetParents = function (indexs) {
        var query = {
            $or: [],
            deleted: 0
        };
        for (var i = 0; i < indexs.length; i++) {
            query.$or.push({ index: indexs[i] });
        }
        return query;
    };
    FileRepository.prototype.getParentsIndex = function (index) {
        var numberOfDot = this.countOccurence(index);
        var result = [index];
        if (numberOfDot === 0) {
            return result;
        }
        else {
            for (var i = 1; i <= numberOfDot; i++) {
                result.push(index.substr(0, (i === 1 ? 4 : (i * 4 + i - 1))));
            }
        }
        return result;
    };
    FileRepository.prototype.countOccurence = function (index) {
        var count = 0;
        for (var i = 0; i < index.length; i++) {
            // tslint:disable-next-line:triple-equals
            // tslint:disable-next-line:curly
            if (index[i] === '.')
                count++;
        }
        return count;
    };
    FileRepository.prototype.insert = function (file) {
        var newFile = new models_1.FileModel(file);
        return newFile
            .save()
            .then(function (savedFile) {
            // tslint:disable-next-line:semicolon
            return Promise.resolve(savedFile.toObject());
        })["catch"](function (err) { return Promise.reject({
            message: err.message
        }); });
    };
    FileRepository.prototype.update = function (file) {
        return models_1.FileModel
            .findOne({
            $and: [
                { _id: file.id }
            ]
        }).then(function (responsedFile) {
            if (responsedFile) {
                responsedFile.name = file.name;
                return responsedFile.save()
                    .then(function (fileUpdate) {
                    return Promise.resolve(responsedFile.toObject());
                })["catch"](function (err) {
                    return Promise.reject({
                        message: err.message
                    });
                });
            }
            else {
                return Promise.reject({
                    message: 'Does not exist.'
                });
            }
        })["catch"](function (err) {
            return Promise.reject({
                message: err.message
            });
        });
    };
    FileRepository.prototype["delete"] = function (id) {
        return models_1.FileModel
            .findOne({ _id: id })
            .then(function (responsedFile) {
            if (responsedFile) {
                return models_1.FileModel.update({
                    index: {
                        $regex: "^" + (responsedFile && responsedFile.index ? responsedFile.index : ''),
                        $options: 'm'
                    }
                }, { $set: { deleted: true } }, { multi: true })
                    .then(function () {
                    return Promise.resolve({
                        message: 'Deleted'
                    });
                })["catch"](function (err) {
                    return Promise.reject({
                        message: err.message
                    });
                });
            }
            else {
                return Promise.reject({
                    message: 'Does not exist.'
                });
            }
        })["catch"](function (err) {
            return Promise.reject({
                message: err.message
            });
        });
    };
    FileRepository.prototype.getAllFiles = function (search) {
        return models_1.FileModel
            .findOne({ _id: (search.parentId === 'root' || !search.parentId ? '5349b4ddd2781d08c09890f3' : search.parentId) })
            .then(function (responsedFile) {
            var nextIndexLv = '9999';
            var maxLength = 5;
            var q = {};
            if (responsedFile && responsedFile.index) {
                nextIndexLv = responsedFile.index + '.' + nextIndexLv;
                maxLength = nextIndexLv.length + 1;
            }
            q = {
                index: {
                    $regex: "^" + (responsedFile && responsedFile.index ? responsedFile.index : ''),
                    $options: 'm'
                },
                $text: { $search: search.search },
                $where: 'this.index.length < ' + maxLength,
                deleted: false
            };
            if (responsedFile && responsedFile._id) {
                q['_id'] = { $ne: responsedFile._id };
            }
            if (!responsedFile) {
                delete q['index'];
            }
            if (!search.search) {
                delete q['$text'];
            }
            if (!search.parentId) {
                q = {
                    $text: { $search: search.search },
                    deleted: false
                };
            }
            return models_1.FileModel.find(q, { content: 0 })
                .sort({
                index: -1
            })
                .then(function (files) {
                return Promise.resolve(files);
            })["catch"](function (err) {
                return Promise.reject({
                    message: err.message
                });
            });
        })["catch"](function (err) {
            return Promise.reject({
                message: err.message
            });
        });
    };
    FileRepository.prototype.getIndexNext = function (parentId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getParentFolder(parentId)
                .then(function (folder) {
                return new Promise(function (res, rej) {
                    var indexOfFolder = folder ? folder.index : null;
                    _this.getMaxIndexFile(indexOfFolder).then(function (indexFile) {
                        res(indexFile);
                    })["catch"](function (err) {
                        rej(err);
                    });
                });
            }).then(function (maxIndex) {
                var indexElements = maxIndex.split('.');
                var lastIndex = indexElements[indexElements.length - 1];
                // tslint:disable-next-line:radix
                var numberIndex = parseInt(lastIndex);
                numberIndex = numberIndex + 1;
                var rsindex = numberIndex.toString();
                var result = maxIndex.substring(0, maxIndex.length - 4) + rsindex.padStart(4, '0');
                resolve(result);
            })["catch"](function (err) {
                reject({
                    message: err.message
                });
            });
        });
    };
    FileRepository.prototype.getParentFolder = function (parentId) {
        return new Promise(function (resolve, reject) {
            if (!parentId) {
                return resolve(null);
            }
            models_1.FileModel.findOne({
                _id: parentId,
                deleted: false
            }, function (err, parent) {
                if (err) {
                    return reject({
                        message: err.message
                    });
                }
                resolve(parent ? parent.toObject() : null);
            });
        });
    };
    FileRepository.prototype.getMaxIndexFile = function (index) {
        return new Promise(function (resolve, reject) {
            var maxLength = index ? index.length + 5 : 5;
            var q = {
                index: {
                    $regex: "^" + index,
                    $options: 'm'
                },
                $where: 'this.index.length < ' + (maxLength + 1),
                deleted: false
            };
            if (!index) {
                delete q.index;
            }
            models_1.FileModel.findOne(q)
                .sort({
                index: -1
            }).exec(function (err, file) {
                if (err) {
                    return reject({
                        message: err.message
                    });
                }
                // if exist max index
                file = file ? file : null;
                if (file && file.index) {
                    var length_1 = index ? index.length : 0;
                    var numberSubstring = index ? 5 : 4;
                    var maxIndex = file.index;
                    maxIndex = maxIndex.length >= length_1 + 4 ? maxIndex.substring(0, length_1 + numberSubstring) : maxIndex;
                    if (maxIndex === index) {
                        return resolve(index + '.0000');
                    }
                    return resolve(maxIndex);
                }
                resolve(file ? file.index : '0000');
            });
        });
    };
    FileRepository.prototype.saveFile = function (nFile) {
        return new Promise(function (resolve, reject) {
            nFile.save(function (err, newFile) {
                if (err) {
                    return reject(err);
                }
                delete newFile.content;
                var rs = newFile ? newFile.toObject() : null;
                resolve(rs);
            });
        });
    };
    return FileRepository;
}());
exports.FileRepository = FileRepository;
