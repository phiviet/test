"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
exports.__esModule = true;
__export(require("./file.repository"));
var repositories_1 = require("../repositories");
var DBContext = /** @class */ (function () {
    function DBContext() {
        this.fileRepository = new repositories_1.FileRepository();
    }
    DBContext.getInstance = function () {
        if (!DBContext.instance) {
            DBContext.instance = new DBContext();
        }
        return DBContext.instance;
    };
    DBContext.prototype.getFileRepository = function () {
        return this.fileRepository;
    };
    return DBContext;
}());
exports.dbContext = DBContext.getInstance();
