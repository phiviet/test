export interface IError {
    statusCode: number;
    message: string;
}

export interface IResponse {
    message: string;
}

export interface IRecoveryRequest {
    deviceId: string;
    appUri: string;
    packageName: string;
}

export interface ISearch {
    parentId?: string;
    search?: string;
}

export class Extension {
    constructor() {
        this.stringPadStart();
    }

    stringPadStart() {
        if (!String.prototype.padStart) {
            String.prototype.padStart = function padStart(targetLength, padString) {
                // tslint:disable-next-line:no-bitwise
                targetLength = targetLength >> 0; // floor if number or convert non-number to 0;
                padString = String(padString || ' ');
                if (this.length > targetLength) {
                    return String(this);
                }
                // tslint:disable-next-line:one-line
                else {
                    targetLength = targetLength - this.length;
                    if (targetLength > padString.length) {
                        // tslint:disable-next-line:max-line-length
                        padString += padString.repeat(targetLength / padString.length); // append to original to ensure we are longer than needed
                    }
                    return padString.slice(0, targetLength) + String(this);
                }
            };
        }
    }
}
