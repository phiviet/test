"use strict";
exports.__esModule = true;
var tika = require('tika');
function getMetaData(uri, options) {
    options = options || [];
    return new Promise(function (resolve, reject) {
        tika.text(uri, options, function (e, content) {
            if (e) {
                return reject(e);
            }
            // tslint:disable-next-line:prefer-const
            var response = {
                text: content
            };
            tika.meta(uri, options, function (err, res) {
                if (err) {
                    return reject(err);
                }
                response['meta'] = res;
                resolve(response);
            });
        });
    });
}
exports.getMetaData = getMetaData;
