"use strict";
var _this = this;
exports.__esModule = true;
var mongoose = require("mongoose");
var fileSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    path: {
        type: String,
        "default": ''
    },
    content: {
        type: String,
        "default": '\n'
    },
    format: {
        type: String
    },
    index: {
        type: String,
        required: true
    },
    deleted: {
        type: Boolean,
        "default": 0
    },
    isFile: {
        type: Boolean,
        "default": 0
    }
});
fileSchema.index({ content: 'text', name: 'text' });
fileSchema.virtual('id').get(function () { return _this._id.toHexString(); });
exports.FileModel = mongoose.model('file', fileSchema);
