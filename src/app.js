"use strict";
exports.__esModule = true;
var express = require("express");
var dotenv = require("dotenv");
var bodyParser = require("body-parser");
var morgan = require("morgan");
var mongoose = require("mongoose");
// import {  } from 'express-fileupload';
var fileUpload = require('express-fileupload');
var shared_1 = require("./shared");
var routes_1 = require("./routes");
var middleware_1 = require("./middleware");
process.env['MONGODB_URI'] = 'mongodb://localhost:27017/quanlychitieu';
// tslint:disable-next-line:no-unused-expression
new shared_1.Extension();
var app = express();
var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, x-access-token');
    next();
};
dotenv.load({ path: '.env' });
app.set('port', (process.env.PORT || 8001));
app.set('baseUrl', '/api');
app.use(fileUpload());
app.use(allowCrossDomain);
// express middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('dev'));
// for database
console.log(process.env.MONGODB_URI + 'duy');
mongoose.connect(process.env.MONGODB_URI);
var db = mongoose.connection;
// (<any>mongoose).Promise = global.Promise;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function () {
    console.log('Connected to MongoDB');
    app.use(app.get('baseUrl') + "/file/", routes_1.fileRouter);
    // middleware error handler
    app.use(middleware_1.errorHandler);
    // start server
    app.listen(app.get('port'), function () { return console.log("Server is listening on port " + app.get('port')); });
});
